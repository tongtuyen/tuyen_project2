package com.example.tuyen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectCiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectCiApplication.class, args);
	}

}
